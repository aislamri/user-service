package com.stackroute.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.stackroute.springboot.filter.JWTFilter;

@SpringBootApplication
public class SpringJwtDemoApplication {
	
	@Bean
	public FilterRegistrationBean<JWTFilter> jwtFilter() {
		FilterRegistrationBean<JWTFilter> filterRegistrationBean = new FilterRegistrationBean<JWTFilter>();
		filterRegistrationBean.setFilter(new JWTFilter());
		filterRegistrationBean.addUrlPatterns("/api/v1/*");
		
		return filterRegistrationBean;
		
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringJwtDemoApplication.class, args);
		System.out.println("Application is booted....");
	}

}
