package com.stackroute.springboot.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.stackroute.springboot.exception.UserNotFoundException;
import com.stackroute.springboot.model.User;
import com.stackroute.springboot.service.UserService;

@RestController
@RequestMapping("api/v1")
public class UserController {

	
	private UserService userService;
	private ResponseEntity<?> responseEntity;

	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	
	
	
	@GetMapping("/users")
	public ResponseEntity<?> getAllUsers(){
		try {
			List<User> usersList = this.userService.getAllUsers();
			this.responseEntity = new ResponseEntity<>(usersList,HttpStatus.OK);
		} catch (Exception e) {
			this.responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return this.responseEntity;
	}
	
	@GetMapping("/user")
	public ResponseEntity<?> getUserByName(@RequestParam String name){
		try {
			User user = this.userService.getUserByName(name);
			this.responseEntity = new ResponseEntity<>(user,HttpStatus.OK);
		} catch (Exception e) {
			this.responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return this.responseEntity;
	}
	
	@GetMapping("/userbyid")
	public ResponseEntity<?> getUserById(@RequestParam String id) throws UserNotFoundException{
		try {
			User userbyid = this.userService.getUserById(id);
			this.responseEntity = new ResponseEntity<>(userbyid,HttpStatus.OK);
		} 
		catch (UserNotFoundException e) {
			throw e;
		}
		catch (Exception e) {
			this.responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return this.responseEntity;
	}
	
	
	@PostMapping("/updateuser")
	public ResponseEntity<?> updateUser(@RequestBody User user) throws UserNotFoundException {
		System.out.println("Called update course .......");
		try {
			User updatedUser = this.userService.updateUser(user);
			responseEntity = new ResponseEntity<>(updatedUser, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again!!!",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	
	@PostMapping("/deleteuser")
	public ResponseEntity<?> deleteUser(@RequestParam("id") String userId) throws UserNotFoundException {
		System.out.println("Called delete user by id .......");
		boolean deletionStatus = false;
		try {
			deletionStatus = this.userService.deleteUser(userId);
			if (deletionStatus) {
				Map<String, String > map = new HashMap<String, String>();
				map.put("deleteStatus", "True");
				map.put("message", "User deleted with the id: " + userId);
				responseEntity = new ResponseEntity<>(map, HttpStatus.ACCEPTED);
			} else
				responseEntity = new ResponseEntity<>("Failed! There is no user with this id. Please try again!!!",
						HttpStatus.NO_CONTENT);
		} catch (UserNotFoundException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again!!!",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	
}
