package com.stackroute.springboot.model;

import java.util.List;

public class AttemptedQuizzes {
	
	private String id;
    private String title;
    private List<Question> questions = null;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}



}
